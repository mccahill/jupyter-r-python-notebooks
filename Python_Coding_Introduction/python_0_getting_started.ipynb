{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting Started\n",
    "\n",
    "This lesson will help you get started using the two main tools of the Python module: the Spyder Integrated Development Environment (IDE) and Jupyter Notebooks.\n",
    "\n",
    "\n",
    "# Jupyter Notebooks\n",
    "\n",
    "All of the lessons in this course are presented through Jupyter Notebooks.  Jupyter Notebooks are a useful teaching tool because they allow you to combine text, code, and the output of functions all in once place.  As an instructor, that means I can give you an explanation or set of instructions of how to code in Python, and you can try out what I suggest in the same document.  \n",
    "\n",
    "Jupyter notebooks can be run on multiple systems, but we are hosting all of the Jupyter Notebooks related to this online review in one place.  You gained access to this place when you reserved your \"docker container\", which is essentially a self-contained execution environment that allows us you to run your own instance of software without having to build an entire virtual machine.  Anything you type and save in one of these notebooks will be saved in your own container so that you can pause at any time, come back later, and pick up where you left off.  Nobody will see your versions of the notebooks except for you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Copying, Renaming, and Moving Jupyter Notebooks\n",
    "\n",
    "One of the greatest advantages of Jupyter notebooks is that they are interactive and able to be edited by both instructors and students.  However, this feature can occassionally be a disadvantage, because it makes it possible for you to accidentally delete or rearrange the text we provide for you.  Therefore, before using a notebook, we recommend that you copy it so that you can retain a copy of the original untouched content.  To copy a notebook, go to the directory with your files, check the box next to the notebook you want to copy, and click the \"duplicate\" button on the top.  \n",
    "\n",
    "<img src='./img/0.12.png'>\n",
    "\n",
    "This will produce a copy.  You can rename this copy by checking the box next to the notebook, clicking rename, and entering a new name.  \n",
    "\n",
    "You may find it helpful to organize your notebooks into folders.  To do this, begin by creating a folder.  Click on the \"New\" button in the upper right hand corner, and choose \"folder\" from the drop down.\n",
    "\n",
    "<img src='./img/make_folder.png'>\n",
    "\n",
    "You can then rename the folder the same way you rename a notebook.  Check the box next to the notebook, click rename, and enter a new name.\n",
    "\n",
    "<img src='./img/rename_folder.png'>\n",
    "\n",
    "The Jupyter environment does not let you drag and drop files into folders the way some operating systems, like Windows, do.  Therefore, to move a file into a folder, you have to change the name of the file to include the path of the folder you want the file to be in.  To do this, type \"/\" followed by the folder name you want the file to be moved into, followed by another \"/\", followed by the actual file name.  For example, the screen shot below shows how you could move the file \"Python_placeholder\" to a folder called \"Original_Versions\":\n",
    "\n",
    "<img src='./img/put_file_in_new_folder.png'> \n",
    "\n",
    "To move back and forth between folders, you can click on different folders in the file path at the top of the screen.  So for example, you could click the folder circled in red below to go to the \"Python_Coding_Introduction\" folder.  \n",
    "<img src='./img/navigate_jupyter_folders.png'> \n",
    "In addition, clicking the symbol circled in purple above will always bring you back to your home directory, which is the one that holds the \"jupyter-r-python-notebooks\" folder."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Using Jupyter Notebooks\n",
    "\n",
    "To use a notebook from your container, simply click on it, and it will appear in a tab in your web browser.  There's a lot you can do with Jupyter Notebooks, all of which is detailed in the \"Notebook Help\" section of the \"Help\" menu.  Here I will highlight the features that are most relevant to this summer online review. \n",
    "\n",
    "The first thing you need to know is that notebooks are divided up into sections called cells.  Cells can contain text (using what's known as markdown language - it's a way of indicating how you want the text styled) or code (which can be executed right in the notebook). The example below shows a notebook with three cells: one with text, and two with code:\n",
    "\n",
    "<img src='./img/jupyter_overview1.png'>\n",
    "\n",
    "Code cells can be differentiated from text cells by a couple visual features.  Most importantly, they will have the word \"In\" written in blue on the left-hand side, followed by a number in brackets.  In addition, the cell will likely have a gray background.  My written instructions generally include both text and code.\n",
    "\n",
    "If you double-click on one of the cells I have provided for you, the cell will suddenly look very different.  Don't panic!  When you double click on a cell, it puts it in editing mode (indicated by a green line on the left-hand side).  What you are seeing is the raw text, Markdown syntax, and code that is working behind the scenes of the pretty renderings you saw when you first opened the notebook.  When you see a green line on the left-hand side, simply place your cursor somewhere in the cell and press the \"Run\" button at the top of the screen to get the pretty version back:\n",
    "\n",
    "<img src='./img/run_button.png'>\n",
    "\n",
    "<u>**Especially if you have never tried programming in Python before, it is a good idea to practice writing Python syntax as often as possible. Towards that end, you may want to follow these steps every time my instructions include code:**</u>\n",
    "\n",
    ">  1) Insert a new code cell below the instructions you are reading.  To do this, click on the text with the code until a blue line appears at the left, indicating the cell is selected.  Then go to the \"Insert\" menu at the top of the screen and choose to insert a cell above or below the cell you are reading.  \n",
    "   2) Put your cursor in the new cell, go to the dropdown menu circled in red in the picture below, and choose \"Code\" to make the new cell a code cell.  The cell should have a blue \"In\" written on the left-hand side.  \n",
    "   3) Type in the code I provided, <mark>**letter for letter rather than copying and pasting**</mark>.  If you type the code in letter for letter throughout the examples provided in the notebook, you will feel much more comfortable writing code from scratch for the quiz questions.  \n",
    "   4) Then run the code by pressing the \"Run\" button.  The result of the code will appear to the right of the word \"Out\" written in red text.  If you typed in the code correctly, the output from your cell should be identical to the output from the original cell I included.  If you typed the code incorrectly, you might get an error message that will give you clues about what was wrong with your syntax.\n",
    "\n",
    "If you want to move the cell you made to a new place, you can use the up and down arrows or the cut/copy/paste buttons indicated in the picture below:\n",
    "   \n",
    "<img src='./img/jupyter_overview2.png'>\n",
    "\n",
    "To delete a cell, place you cursor in it, go to the \"Edit\" menu, and choose \"Delete Cells\".  \n",
    "\n",
    "Make sure you save your work as you go along by choosing \"Save and Checkpoint\" from the \"File\" menu.  If you feel like you messed things up in your notebook, you can also use the \"Revert to Checkpoint\" option from the \"File\" menu to return to an earlier version.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Practice\n",
    "\n",
    "I recommend practicing adding a cell below the text you are reading right now to a get a feeling for how to do it.  In fact, try adding a couple of them and then reorder them using the arrow buttons.  Do you have the hang of it?  Great!  You are almost ready for the next lesson.  We just need to learn about one more tool."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using Virtual Machines to Run Spyder\n",
    "\n",
    "Spyder is a powerful development environment for Python programming that makes writing programs much easier than using a text editor.  <mark>**For this course, we intend for you to develop all the code you will write to answer the quizzes and exam in Spyder.  We also recommend that you practice replicating the examples from the provided Jupyter notebooks in Spyder as you go along.**  \n",
    "\n",
    "Sypder comes free with the [Anaconda](https://www.anaconda.com/download/) distribution of Python. While you are welcome to install Anaconda on your own personal computers, we provide instructions assuming you will use your Duke virtual machine (VM).  Please see the instructions provided in the \"Tools used in this course\" section of the course Sakai website to learn how to access your VM.   \n",
    "\n",
    "Once you have launched your remote desktop connection to your VM, the desktop of the VM should look like this:\n",
    "\n",
    "<img src='./img/0.4.png'>\n",
    "\n",
    "It may ask you to perform Windows updates, if so feel free to do so.\n",
    "\n",
    "Double click on the icon on the left called \"Software Center\" and you'll see a list of software that can be installed:\n",
    "\n",
    "<img src='./img/0.5.png'>\n",
    "\n",
    "If you haven't done so already, scroll down until you see \"Anaconda 5.2.0 py 3.6 - Install Anaconda 5.2.0 w/ Python 3.6\". Check the box to the left of it and click \"INSTALL SELECTED\".  (Note that you will not see Anaconda in this list if you have installed it already.)\n",
    "\n",
    "<img src='./img/0.7.png'>\n",
    "\n",
    "It will take some time to install.\n",
    "\n",
    "# Launching Spyder\n",
    "Once Anaconda is installed, you're ready to begin Python coding in Spyder. Click the Windows icon in the bottom left corner of your VM desktop and scroll until you find \"Anaconda3\".  When you click on the Anaconda icon, you'll see options for a command prompt, Jupyter Notebook, and most relevant here, Spyder. \n",
    "\n",
    "<img src='./img/0.9.png'>\n",
    "\n",
    "Click Spyder to launch Spyder. When you do, you should see the window below.   \n",
    "\n",
    "<img src='./img/0.10.png'>\n",
    "\n",
    "After watching this video about how to use Sypder (also provided in the \"Tools used in this course\" section of the Sakai website):\n",
    "\n",
    "https://warpwire.duke.edu/w/ExcCAA/\n",
    "\n",
    "...you're ready to start coding!  Move on to the next notebook to write your first lines of code!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
